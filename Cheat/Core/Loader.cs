﻿using ClassLibrary1.Cheat.Managers;
using UnityEngine;

namespace ClassLibrary1 {
    public class Loader : MonoBehaviour {
        public static void Load() {
            GameObject cheat = null;
            if (cheat == null) {
                cheat = new GameObject();
                cheat.AddComponent<Console>();
                cheat.AddComponent<ModuleManager>();
                cheat.AddComponent<Overlay>();
                cheat.AddComponent<ClickGUI>();
                cheat.AddComponent<KeybindManager>();
                UnityEngine.Object.DontDestroyOnLoad(cheat);
            }
        }
    }
}