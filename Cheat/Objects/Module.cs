﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ClassLibrary1.Cheat.Managers;
using UnityEngine;

namespace ClassLibrary1.Cheat.Objects {
    public abstract class Module {
        public Dictionary<Option, FieldInfo> options = new Dictionary<Option, FieldInfo>();
        
        public Module() {
            foreach (var fieldInfo in GetType().GetFields()) {
                Option o = fieldInfo.GetCustomAttributes(typeof(Option), false).FirstOrDefault() as Option;
                if (o != null) {
                    o.type = fieldInfo.FieldType.Name;
                    o.inst = this;
                    options.Add(o, fieldInfo);
                }
            }

        foreach (var methodInfo in GetType().GetMethods()) {
                    Command o = methodInfo.GetCustomAttributes(typeof(Command), false).FirstOrDefault() as Command;
                    if (o != null) {
                        o.invokeinstance = this;
                        o.MethodInfo = methodInfo;
                        CommandManager.Commands.Add(o);
                    }
                }
        
        }

        public abstract void onEnabled();
        public abstract void onDisabled();
        public abstract void onToggled();
        public abstract void onUpdate();
        public abstract void onGUI();

    }

    
    
    public enum Category {
        None, Aim
    }
    
    

    public class Argument {
        public string Name;
        public string about;
        public string usage;
        public bool required;
        
        public Argument(string name, string about, string usage, bool req = false) {
            this.Name = name;
            this.about = about;
            this.usage = usage;
            this.required = req;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class Command : Attribute {
        public String command;
        public String usage;
        public String About;
        public Argument[] Arguments;
        //this will be set from the consolemanager or somthing when commands are found
        public MethodInfo MethodInfo;
        public object invokeinstance;
        
        public Command(String command, string usage, string about, Argument[] args = null) {
            this.About = about;
            this.command = command;
            this.usage = usage;
            this.Arguments = args;
        }
        
        //argumentname: value
        public void execute(Dictionary<String, string> args) {
            MethodInfo.Invoke(invokeinstance, new object[1]{args});
        }
    }
    
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class Option : Attribute {
        public readonly String Name;
        public readonly float[] minMax;
        public readonly string[] ComboBox;
        public  string type;
        public object inst;
        public object tempval = "";
        public Option(string name, float[] minMax = null) {
            this.Name = name;
            this.minMax = minMax;
            
        }
        
    }
    
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ModuleInfo : Attribute {
        public readonly string Name;
        public bool enabled = false;
        public Category Category;
        public KeyCode KeyCode;
        public bool expanded = false;
        
        
        public ModuleInfo(string Name, Category Category = Category.None, KeyCode key = KeyCode.None) {
            this.Name = Name;
            this.Category = Category;
            this.KeyCode = key;
        }

        public bool Enable() {
            if (!enabled) {
                Toggle();
                return true;
            }

            return false;
        }

        public bool Disable() {
            if (enabled) {
                Toggle();
                return true;
            }

            return false;
        }

        public void Toggle() {
            enabled = !enabled;
            
            if (enabled) {
                ModuleManager.Modules[this].onEnabled();
            } else {
                ModuleManager.Modules[this].onDisabled();
            }

            ModuleManager.Modules[this].onToggled();
        }
    }


}