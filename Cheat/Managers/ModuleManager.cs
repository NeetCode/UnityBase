﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassLibrary1.Cheat.Objects;
using UnityEngine;

namespace ClassLibrary1.Cheat.Managers {
    public class ModuleManager : MonoBehaviour {
        public static Dictionary<ModuleInfo, Module> Modules = new Dictionary<ModuleInfo, Module>();
        static ModuleManager() {
            foreach (var potentialmodule in typeof(Loader).Assembly.GetTypes()) {
                try {
                    if (potentialmodule.IsSubclassOf(typeof(Module))) {
                        Module module = (Module) Activator.CreateInstance(potentialmodule);
                        Modules.Add(module.GetType().GetCustomAttributes(typeof(ModuleInfo), false).FirstOrDefault() as ModuleInfo, module);
                    }
                } catch (Exception e) {
                    
                }
            }
        }

        private void Update() {
            foreach (var keyValuePair in Modules) {
                if (keyValuePair.Key.enabled) {
                    keyValuePair.Value.onUpdate();
                }
            }
        }

        private void OnGUI() {
            foreach (var keyValuePair in Modules) {
                if (keyValuePair.Key.enabled) {
                    keyValuePair.Value.onGUI();
                }
            }
        }
    }
}