﻿using UnityEngine;

namespace ClassLibrary1.Cheat.Managers {
    public class KeybindManager : MonoBehaviour {
        private void Update() {
            foreach (var keyValuePair in ModuleManager.Modules.Keys) {
                if (Input.GetKeyDown(keyValuePair.KeyCode)) {
                    keyValuePair.Toggle();
                }
            }

            if (Input.GetKeyDown(KeyCode.G)) {
                Console.show = !Console.show;
            }

            if (Input.GetKeyDown(KeyCode.Y)) {
                ClickGUI.show = !ClickGUI.show;
                
            }
        }
    }
}