﻿using System;
using ClassLibrary1.Cheat.Objects;
using UnityEngine;

namespace ClassLibrary1.Cheat.Modules {
    [ModuleInfo(Name: "Example module", Category: Category.Aim, key:KeyCode.B)]
    public class ExampleModule: Module {

        [Option("Test option of a float", minMax: new float[2]{-10f,5f})] 
        public float angle = 0;


        public override void onEnabled() {
            
        }

        public override void onDisabled() {
        }

        public override void onToggled() {
        }

        public override void onUpdate() {
        }

        public override void onGUI() {
            
        }
    }
}