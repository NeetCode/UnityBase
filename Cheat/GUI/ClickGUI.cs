﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using ClassLibrary1.Cheat.Managers;
using ClassLibrary1.Cheat.Objects;
using UnityEngine;
using UnityEngine.UI;

namespace ClassLibrary1 {
    public class ClickGUI : MonoBehaviour {
        private static int width = 400;
        private static int height = 200;
        private Category Selected = (Category) 0;
        private GUIStyle buttonstyle = new GUIStyle();
        GUIStyle togglestyle = new GUIStyle();
        private Rect clickguir = new Rect(0,0,width,height);
        private Rect clicktitle = new Rect(0,0,1000,20);
        private void OnEnable() {
            buttonstyle.normal.background = new Texture2D(1,1);
            togglestyle.normal.background = new Texture2D(1,1);
            togglestyle.active.background = new Texture2D(1,1);
            buttonstyle.active.background = new Texture2D(1,1);
            togglestyle.normal.background.SetPixel(1,1, Color.black);
            buttonstyle.normal.background.SetPixel(1,1, Color.blue);
            togglestyle.normal.background.Apply();
            buttonstyle.normal.background.Apply();
        }

        public static bool show = false;
        private void OnGUI() {
            if(show)
            GUILayout.Window(23123, clickguir, renderwindow,"clickgui");
            
            
        }
        
        void cheattoggle(Rect bounds, ModuleInfo i   ) {
            Rect cheatrect = bounds;
            cheatrect.width = cheatrect.width - 20;
            Rect toggle = new Rect(cheatrect.x+cheatrect.width,bounds.y, 20,bounds.height);
            int controlID = GUIUtility.GetControlID(cheatrect.GetHashCode(), FocusType.Passive);
            int toggleid = GUIUtility.GetControlID(toggle.GetHashCode(), FocusType.Passive);

            if (Event.current.type == EventType.Repaint) {
                buttonstyle.Draw(cheatrect, new GUIContent("testdd"), controlID );
                togglestyle.Draw(toggle, new GUIContent("testff"), toggleid);
            }

            switch (Event.current.GetTypeForControl(toggleid)) {
                case EventType.MouseUp: {
                    if (cheatrect.Contains(Event.current.mousePosition)) {
                        Console.print("toggle cheat");
                        i.Toggle();
                    }//d
                    else  if (toggle.Contains(Event.current.mousePosition)) {
                        Console.print("toggled");
                        i.expanded = !i.expanded;
                    }
                    break;
                }
            }
        }

        public bool d = false;
        void renderwindow(int id) {
            Selected = (Category) GUILayout.Toolbar((int) Selected, Enum.GetNames(typeof(Category)));
            
            foreach (var keyValuePair in ModuleManager.Modules) {
                if (keyValuePair.Key.Category == Selected) {
                     cheattoggle(GUILayoutUtility.GetRect(new GUIContent("cheat"), GUIStyle.none), keyValuePair.Key);
                    if (keyValuePair.Key.expanded ) {
                        foreach (var valueOption in keyValuePair.Value.options) {
                            if (Event.current.type != EventType.MouseUp) {
                                GUILayout.BeginHorizontal();

                                GUILayout.Label(valueOption.Key.Name);
                                switch (valueOption.Key.type) {
                                    case "Single": {
                                        keyValuePair.Value.GetType().InvokeMember(valueOption.Value.Name,
                                            BindingFlags.SetField, null,
                                            valueOption.Key.inst, new float[] {
                                                GUILayout.HorizontalSlider(
                                                    (float) valueOption.Value.GetValue(valueOption.Key.inst)
                                                    , valueOption.Key.minMax[0], valueOption.Key.minMax[1])
                                            }.Cast<object>().ToArray());
                                        break;
                                    }
                                    case "Int32": {
                                        break;
                                    }
                                    case "String": {
                                        string ssid = "stringset" + valueOption.Key.Name + valueOption.Value.Name;
                                        GUI.SetNextControlName(ssid);
                                        valueOption.Key.tempval =
                                            GUILayout.TextField(valueOption.Key.tempval.ToString());
                                        if (Event.current.isKey && Event.current.keyCode == KeyCode.Return &&
                                            GUI.GetNameOfFocusedControl() == ssid) {
                                            valueOption.Value.SetValue(valueOption.Key.inst, valueOption.Key.tempval);
                                            valueOption.Key.tempval = "";
                                        }

                                        break;
                                    }
                                    default: {
                                        GUILayout.Label(valueOption.Key.type);
                                        break;
                                    }
                                }

                                GUILayout.EndHorizontal();
                            }
                        
                    }
                    }
                }
            }
            GUI.DragWindow(clicktitle);
        }
    }
}