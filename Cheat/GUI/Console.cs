﻿using System.Collections.Generic;
using ClassLibrary1.Cheat.Managers;
using UnityEngine;

namespace ClassLibrary1 {
    public class Console: MonoBehaviour {
        private const int margin = 20;
        Rect windowRect = new Rect(margin,margin,Screen.width/2-(margin*2), Screen.height/2-(margin*2));
        Rect titlebarrect = new Rect(0,0,1000,20);

        struct logentry {
            public string message;
            public string stacktrace;
            public LogType type;
        }

        public static bool show = false;
        
        private void OnEnable() {
            Application.RegisterLogCallback(callback);
        }

        private void OnGUI() {
            if(show)
            windowRect = GUILayout.Window(13444, windowRect, consoewindow, "console");
        }

        static  List<logentry> logs = new List<logentry>();
        Vector2 scrollPosition;
        

        private static Dictionary<LogType, Color> logcolors = new Dictionary<LogType, Color>() {
            { LogType.Assert, Color.cyan },
            { LogType.Error, Color.red },
            { LogType.Exception, Color.magenta },
            { LogType.Log, Color.white },
            { LogType.Warning, Color.yellow },
        };

        private string command = "";

        static void print(string m) {
            print(LogType.Log, m);
        }
        
        static void print(LogType type, string m) {
            logs.Add(new logentry() {
                message = m,type = type
            });
        }
        
        void consoewindow(int windowid) {

            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            foreach (var logentry in logs) {
                GUI.contentColor = logcolors[logentry.type];
                GUILayout.Label(logentry.message);
            }
            GUILayout.EndScrollView();
            
            GUI.contentColor = Color.white;
            GUILayout.BeginHorizontal();
            if (Event.current.isKey && Event.current.keyCode == KeyCode.Return && GUI.GetNameOfFocusedControl() == "consolein") {
                CommandManager.HandleInput(command);
                command = "";
            }
            GUI.SetNextControlName("consolein");
            command = GUILayout.TextField(command);
            GUILayout.EndHorizontal();
            GUI.DragWindow(titlebarrect);
        }

        void callback(string message, string stack, LogType type) {
            logs.Add(new logentry() {
                message = message, stacktrace = stack, type = type
            });
        }
    }
}